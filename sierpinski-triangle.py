import turtle
import os

myPen = turtle.Turtle()
myPen.ht()
myPen.speed(5)
myPen.pencolor('red')

points = [[-175, -125], [0, 175], [175, -125]]


def get_mid(p1, p2):
    return (p1[0]+p2[0])/2, (p1[1]+p2[1])/2


def triangle(points1, depth):
    myPen.up()
    myPen.goto(points1[0][0], points1[0][1])
    myPen.down()
    myPen.goto(points1[1][0], points1[1][1])
    myPen.goto(points1[2][0], points1[2][1])
    myPen.goto(points1[0][0], points1[0][1])
    if depth > 0:
        triangle([points1[0], get_mid(points1[0], points1[1]), get_mid(points1[0], points1[2])], depth-1)
        triangle([points1[1], get_mid(points1[0], points1[1]), get_mid(points1[1], points1[2])], depth-1)
        triangle([points1[2], getMid(points1[2], points1[1]), getMid(points1[0], points1[2])], depth-1)


triangle(points, 4)
os.system("pause")
